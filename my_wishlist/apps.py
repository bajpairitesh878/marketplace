from django.apps import AppConfig


class MyWishlistConfig(AppConfig):
    name = 'my_wishlist'
