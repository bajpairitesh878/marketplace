# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext as _

from django.contrib.auth import get_user_model
from django.db import models

from utils.models import ModelMixin

User = get_user_model()

USER_LANGUAGE = (
        ('EN', 'ENGLISH'),
        ('HI', 'हिंदी '),
    )

STAFF_TYPE = (
        ('1', _('Company Admin')),
        ('2', _('Branch Admin')),
        ('3', _('Filed Executive')),
    )


class UserProfile(models.Model):
    """
    User Profile model store the basic user information
    """
    user = models.OneToOneField(User, related_name='profile', on_delete=models.CASCADE)
    type = models.CharField(max_length=30, choices=STAFF_TYPE, blank=True, null=True)
    mobile = models.CharField(max_length=15, blank=True, null=True)
    profile_pic = models.ImageField(blank=True, null=True, help_text=_('Upload your profile pic'))
    membership_status = models.BooleanField(default=False)
    fullName = models.CharField(null=True, blank=True, max_length=200)
    address = models.CharField(null=True, blank=True, max_length=200)
    state = models.CharField(null=True, blank=True, max_length=200)
    city = models.CharField(null=True, blank=True, max_length=200)

    def save(self, *args, **kwargs):
        self.fullName = self.user.first_name + ' ' + self.user.last_name
        return super().save(*args, **kwargs)


class Otp(models.Model):
    OTP_VERIFY = (
        ('true', 'True'),
        ('false', 'False'),
    )
    otp = models.IntegerField(default=0)
    user = models.ForeignKey(
        User, related_name='user_otp', on_delete=models.CASCADE, null=True)
    verify = models.CharField(choices=OTP_VERIFY, default='false', max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
