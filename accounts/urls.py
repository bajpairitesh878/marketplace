# import djoser.views
from django.conf.urls import url
from django.contrib.auth import get_user_model
from rest_framework import routers
from django.urls import path, include
from .views import *

router = routers.DefaultRouter()
User = get_user_model()


urlpatterns = [
    path('', include(router.urls)),
    path('user', UserProfileView.as_view()),
    path('user/update', UserProfileUpdateView.as_view()),
    path('otp/send', OTPView.as_view()),
    path('reset/password', PasswordResetView.as_view()),
    path('change/password', PasswordChangeView.as_view()),


]
