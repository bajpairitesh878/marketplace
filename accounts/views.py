import random

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions, status
from rest_framework.authentication import TokenAuthentication
from .serializers import *
from rest_auth.views import LoginView
from rest_auth.registration.views import RegisterView
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model


User = get_user_model()


class UserProfileView(APIView):
    authentication_classes = (TokenAuthentication,)

    def get(self, request):
        query = User.objects.get(pk=request.user.pk)
        serializer = UserDetailsSerializer(query)
        return Response(serializer.data)


class LoginApiView(LoginView):
    authentication_classes = (TokenAuthentication,)


class RegisterApiView(RegisterView):
    authentication_classes = (TokenAuthentication,)


class UserProfileUpdateView(APIView):
    authentication_classes = (TokenAuthentication,)

    def put(self, request):
        query_user = User.objects.get(pk=request.user.pk)
        query_profile = UserProfile.objects.get(user=request.data['user'])
        serializer_user = UserSerializer(query_user, data=request.data)
        if serializer_user.is_valid():
            serializer_user.save()
        serializer_profile = ProfileSerializer(query_profile, data=request.data)
        if serializer_profile.is_valid():
            serializer_profile.save()
            return Response({'user': serializer_user.data, 'profile':serializer_profile.data})
        return Response(serializer_profile.errors)


class OTPView(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        user = User.objects.filter(email=request.data['email']).last()
        if user:
            otp = random.randint(1111, 9999)
            try:
                subject = 'Om Shanti Store'
                message = f'Hi {user.username}, Here is OTP for password reset.\n{otp}'
                email_from = settings.EMAIL_HOST_USER
                recipient_list = [user.email, ]
                send_mail(subject, message, email_from, recipient_list)
            except:
                print('send email.')
            model_data = {
                'otp': otp,
                'user': user.id,
            }
            serializer = OTPSerializer(data=model_data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors)
        return Response({'message':'There is no user register with this email.'})

    def put(self, request):
        user = User.objects.filter(email=request.data['email']).last()
        if user:
            query_otp = Otp.objects.filter(user=user.id).last()
            if query_otp:
                query_data = {
                    'verify': 'true'
                }
                if int(request.data['otp']) == int(query_otp.otp):
                    query_update = OTPSerializer(query_otp, data=query_data)
                    if query_update.is_valid():
                        query_update.save()
                        return Response({'message':'you have successfully verify OTP.'}, status=status.HTTP_200_OK)
                    return Response({'message': query_update.errors}, status=status.HTTP_400_BAD_REQUEST)
                return Response({'message': 'OTP that you enter is not valid.', 'status': 400}, status=status.HTTP_400_BAD_REQUEST)
            return Response({'message': 'Please send OTP first.'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'message': 'There is no user register with this email.'}, status=status.HTTP_400_BAD_REQUEST)


class PasswordResetView(APIView):
    permission_classes = (permissions.AllowAny,)

    def put(self, request):
        user = User.objects.filter(email=request.data['email']).last()
        if user:
            query_otp = Otp.objects.filter(user=user.id, verify='true').last()
            if query_otp:
                user.set_password(request.data['password'])
                user.save()
                return Response({'message': 'You have successfully reset your password.'}, status=status.HTTP_200_OK)
            return Response({'message': 'First Verify OTP then reset your password'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'message': 'There is no user register with this email.'}, status=status.HTTP_400_BAD_REQUEST)


class PasswordChangeView(APIView):
    authentication_classes = (TokenAuthentication,)

    def put(self, request):
        user = User.objects.filter(username=request.data['username']).last()
        print(user)
        if user:
            user_auth = authenticate(username=request.data['username'], password=request.data['old_password'])
            print(user_auth)
            if user_auth is not None:
                user.set_password(request.data['password'])
                user.save()
                return Response({'message': 'You have successfully change your password.'}, status=status.HTTP_200_OK)
            return Response({'message': 'Password which you enter is not correct.'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'message': 'There is no user register with this email.'}, status=status.HTTP_400_BAD_REQUEST)
