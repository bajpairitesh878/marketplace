import json
from django.contrib import admin
from .models import *
# Register your models here.


class OrderManagementAdminView(admin.ModelAdmin):
    list_display = ('invoice_no', 'invoice_generate',
                    'invoice_date', 'payment_status')
    list_filter = ('invoice_no',
                   'invoice_date', 'payment_status', 'gst_payable')
    list_display_links = ('invoice_no', 'invoice_date', 'payment_status')
    search_fields = ('invoice_no',
                     'invoice_date', 'payment_status')

    # def changelist_view(self, request, extra_context=None):
    #     total_order = OrderManagement.objects.all()
    #     sub_total = 0
    #     gst = 0
    #     test = 0
    #     profit = 0
    #     for item in total_order:
    #         # check for gst and revenue
    #         if item.gst_payable == 'no':
    #             for product in item.product_detail.all():
    #                 sub_total += product.total
    #                 test += product.total
    #         else:
    #             for product in item.product_detail.all():
    #                 gst += product.total * 18 / 100
    #                 sub_total += gst + product.total
    #                 test += product.total
    #
    #         # check for profit
    #         for product in item.product_detail.all():
    #             profit += product.total - product.total_vendor_price
    #
    #     analysis_data = {
    #         'total': round(sub_total/1000, 1),
    #         'gst': round(gst/1000, 1),
    #         'totale_sale': len(total_order),
    #         'profit': round(profit/1000, 1)
    #     }
    #
    #     # Serialize and attach the chart data to the template context
    #     extra_context = extra_context or {
    #         "analysis_data": analysis_data}
    #     print(extra_context)
    #
    #     # Call the superclass changelist_view to render the page
    #     return super().changelist_view(request, extra_context=extra_context)


admin.site.register(CustomerDetail)
admin.site.register(CompanyDetail)
admin.site.register(OrderManagement, OrderManagementAdminView)
