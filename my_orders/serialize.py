from rest_framework import serializers
from .models import *
from product_app.serializer import *
from accounts.serializers import *


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderManagement
        fields = '__all__'


class OrderDetailSerializer(serializers.ModelSerializer):
    product_detail = ProductSerializer(many=True)
    user = UserDetailsSerializer()

    class Meta:
        model = OrderManagement
        fields = '__all__'