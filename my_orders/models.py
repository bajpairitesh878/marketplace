from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.html import mark_safe
from django.contrib.auth import get_user_model
from product_app.models import *

User = get_user_model()
# Create your models here.


def increment_invoice_number():
    last_invoice = OrderManagement.objects.all().order_by('id').last()
    if not last_invoice:
        return 'RB0001'
    invoice_no = last_invoice.invoice_no
    invoice_int = int(invoice_no.split('RB')[-1])
    new_invoice_int = invoice_int + 1
    new_invoice_no = 'RB000' + str(new_invoice_int)
    return new_invoice_no


class CustomerDetail(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=100, default='')
    streat_detail = models.TextField(default='', null=True, blank=True)
    city = models.CharField(default='', null=True, blank=True, max_length=100)
    state = models.CharField(default='Delhi', null=True,
                             blank=True, max_length=100)
    phone = models.CharField(blank=True, null=True, max_length=100)
    email = models.EmailField(null=True, blank=True, default='')
    gstn = models.CharField(max_length=150, null=True, blank=True, default='')

    def __str__(self):
        return self.name


class CompanyDetail(models.Model):
    company_name = models.CharField(max_length=100, default='')
    company_gstn = models.CharField(
        max_length=150, null=True, blank=True, default='')
    streat_detail = models.TextField(default='', null=True, blank=True)
    company_city = models.CharField(
        default='', null=True, blank=True, max_length=100)
    company_state = models.CharField(default='Delhi', null=True,
                                     blank=True, max_length=100)
    company_phone = models.CharField(blank=True, null=True, max_length=100)
    company_email = models.EmailField(null=True, blank=True, default='')
    invoice_logo = models.ImageField()

    def __str__(self):
        return self.company_name


class OrderManagement(models.Model):
    GST_PAY = [
        ('yes', 'YES'),
        ('no', 'NO')
    ]
    BIL_PAY = [
        ('paid', 'PAID'),
        ('unpaid', 'UNPAID')
    ]
    DELIVERY_STATUS = [
        ('Pending', 'Pending'),
        ('Out for delivery', 'Out for delivery'),
        ('Delivered', 'Delivered')
    ]
    PAYMENT_METHOD = [
        ('Cash On Delivery', 'Cash On Delivery')
    ]

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True)
    CompanyDetail = models.ForeignKey(CompanyDetail, null=True, blank=True, on_delete=models.CASCADE)
    product_detail = models.ManyToManyField(Product, blank=True)
    additional_notes = models.TextField(
        max_length=1500, null=True, blank=True, default='')
    gst_payable = models.CharField(
        choices=GST_PAY, default='no', max_length=100)
    invoice_no = models.CharField(
        max_length=500, default=increment_invoice_number, null=True, blank=True)
    invoice_date = models.DateField(auto_now=True)
    billing_date = models.DateField(auto_now=True)
    delivery_date = models.DateField(auto_now=True)
    payment_status = models.CharField(
        choices=BIL_PAY, default='unpaid', max_length=100)
    delivery_status = models.CharField(choices=DELIVERY_STATUS, max_length=100, default='Pending')
    payment_method = models.CharField(choices=PAYMENT_METHOD, max_length=100, default='Cash On Delivery')
    generate_invoice = models.CharField(
        max_length=300, null=True, blank=True, default='')

    def invoice_generate(self):
        return mark_safe('<a target="_blank" href="%s">%s</a>' % (self.generate_invoice, 'Generate Invoice'))
    invoice_generate.allow_tags = True


@receiver(post_save, sender=OrderManagement)
def my_handler(sender, instance, **kwargs):
    invoice_url = f'/order/admin/print/invoice/{instance.id}'
    print(invoice_url)
    order = OrderManagement.objects.filter(
        id=instance.id).update(generate_invoice=invoice_url)
    print(order)
