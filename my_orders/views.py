from rest_framework.views import APIView
from rest_framework.response import Response

from .serialize import *
from .models import *
from accounts.models import *
from accounts.serializers import *
from product_app.models import *
from product_app.serializer import *
from my_cart.models import *
from rest_framework import permissions

# Create your views here.
class OrderView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, id):
        query = OrderManagement.objects.filter(user=id)
        serializer = OrderDetailSerializer(query, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = OrderSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            query = UserCart.objects.filter(user=request.data['user'])
            cart = [i for i in query]
            inst = OrderManagement.objects.get(pk=serializer.data['id'])
            cart_product = []
            for i in cart: 
                cart_product.append(i.product.last().id)            
            inst.product_detail.add(*cart_product)
            inst.save()
            print('cart', cart_product, inst)
            return Response(serializer.data)
        return Response(serializer.errors)

    def delete(self, request, id):
        query = OrderManagement.objects.filter(pk=id).delete()
        return Response({'message':'Successfully Deleted'})
