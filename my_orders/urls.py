from django.urls import path
from .views import *

urlpatterns = [
    path('user', OrderView.as_view()),
    path('user/<int:id>', OrderView.as_view()),
]