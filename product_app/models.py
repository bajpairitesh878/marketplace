from django.db import models
from django.template.defaultfilters import slugify
from django.urls import reverse
from ckeditor.fields import RichTextField
from django.contrib.auth import get_user_model

User = get_user_model()


# Create your models here.
class ProductImage(models.Model):
    image = models.FileField(null=True, blank=True)


class Category(models.Model):
    category_name = models.CharField(max_length=200, default='')
    category_image = models.FileField(null=True, blank=True, upload_to='category')
    date_created = models.DateField(auto_now=True)

    def __str__(self):
        return self.category_name


class ExtraProduct(models.Model):
    name = models.CharField(max_length=200, default='')
    image = models.FileField(null=True, blank=True, upload_to='category')
    date_created = models.DateField(auto_now=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    category = models.ManyToManyField(Category, blank=True)
    product_name = models.CharField(max_length=250, default='')
    images = models.ManyToManyField(ProductImage, blank=True)
    is_market = models.BooleanField(default=False)
    quantity = models.IntegerField(null=True, blank=True)
    mrp = models.IntegerField()
    discount = models.IntegerField(null=True, blank=True)
    sale_price = models.IntegerField(null=True, blank=True)
    product_description = RichTextField(null=True, blank=True, verbose_name='Product Description', default='')
    product_image = models.FileField(null=True, blank=True, upload_to='product')
    base_image = models.FileField(null=True, blank=True, upload_to='product')
    search_image = models.FileField(null=True, blank=True, upload_to='product')
    thumbnil_image = models.FileField(null=True, blank=True, upload_to='product')
    bestBefore = models.DateField(auto_now=False)

    def save(self, *args, **kwargs):
        if not self.sale_price:
            self.sale_price = self.mrp - self.discount

        return super().save(*args, **kwargs)


class ProductReview(models.Model):
    product = models.ForeignKey(
        Product, null=True, blank=True, on_delete=models.CASCADE)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    email = models.CharField(max_length=100, null=True, blank=True, default='')
    review = models.CharField(
        max_length=500, null=True, blank=True, default='')
    star_count = models.DecimalField(
        null=True, blank=True, verbose_name='Offer Price', default=0, max_digits=10, decimal_places=2)
    created_date = models.DateField(auto_now=True)
