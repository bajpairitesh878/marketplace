from django.contrib import admin
from .models import *

# Register your models here.


class CategoryAdminView(admin.ModelAdmin):
    list_display = ('id', 'category_name', 'date_created')


class ProductAdminView(admin.ModelAdmin):
    list_display = ('id', 'product_name', 'quantity', 'mrp', 'discount', 'sale_price')


admin.site.register(Category, CategoryAdminView)
admin.site.register(Product, ProductAdminView)
admin.site.register(ProductImage)
admin.site.register(ExtraProduct)