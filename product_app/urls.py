from django.urls import path
from .views import *

urlpatterns = [
    path('category', CategoryView.as_view()),
    path('product', ProductView.as_view()),
    path('market/product', MarketProductView.as_view()),
    path('review', ProductReviewView.as_view()),
    path('search', ProductSearchView.as_view()),
    path('review/<int:id>', ProductReviewView.as_view())
]