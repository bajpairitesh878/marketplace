import re
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from .serializer import *
from .models import *
from accounts.models import *
from accounts.serializers import *

# Create your views here.


class CategoryView(APIView):

    def get(self, request):
        query = Category.objects.all()
        serializer = CategorySerializer(query, many=True)
        return Response(serializer.data)


class ProductView(APIView):

    def get(self, request):
        query = Product.objects.all()
        serializer = ProductSerializer(query, many=True)
        return Response(serializer.data)


class MarketProductView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request):
        query = Product.objects.filter(is_market=True)
        serializer = ProductSerializer(query, many=True)
        return Response(serializer.data)


class ProductSearchView(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        query = Product.objects.filter(product_name__icontains=request.data['query'])
        serializer = ProductSerializer(query, many=True)
        return Response(serializer.data)


class ProductReviewView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, id):
        query = ProductReview.objects.filter(product=id)
        data = []
        for i in query:
            serializer = ReviewSerializer(i)
            print(serializer.data['user'])
            profile = UserProfile.objects.get(user=serializer.data['user'])
            profile_serializer = ProfileSerializer(profile)
            review = serializer.data
            review['profile'] = profile_serializer.data
            data.append(review)
        return Response(data)
    
    def post(self, request):
        serializer = ReviewSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)