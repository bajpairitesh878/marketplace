from django.contrib import admin
from .models import *

# Register your models here.
class BannerAdminView(admin.ModelAdmin):
    list_display = ('id', 'is_active', 'slide_no', 'created_date', 'banner_picture')

admin.site.register(Banner, BannerAdminView)