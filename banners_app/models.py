from django.db import models
from django.utils.html import mark_safe

# Create your models here.
class Banner(models.Model):
    banner = models.FileField(null=True, blank=True, upload_to='banners')
    is_active = models.BooleanField(default=True)
    slide_no = models.IntegerField(null=True, blank=True)
    created_date = models.DateField(auto_now=True)

    def banner_picture(self):
        return mark_safe('<img src="/media/%s" width="100" height="100" />' % (self.banner))