from django.shortcuts import render
from rest_framework.views import APIView
from .models import *
from .serializer import *
from rest_framework.response import Response

# Create your views here.

class BannerView(APIView):

    def get(self, request):
        query = Banner.objects.filter(is_active=True).order_by('slide_no')
        serializer = BannerSerializer(query, many=True)
        return Response(serializer.data)