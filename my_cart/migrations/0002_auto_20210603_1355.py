# Generated by Django 3.1.12 on 2021-06-03 13:55

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('product_app', '0004_auto_20210603_1336'),
        ('my_cart', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='UserCar',
            new_name='UserCart',
        ),
    ]
