from django.db import models
from product_app.models import *
from django.contrib.auth import get_user_model

User = get_user_model()


# Create your models here.
class UserCart(models.Model):
    product = models.ManyToManyField(Product)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_purchase = models.BooleanField(default=False)
