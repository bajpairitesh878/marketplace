from django.urls import path
from .views import *


urlpatterns = [
    path('user/<int:id>', CartView.as_view()),
    path('user/delete/<int:id>', CartView.as_view()),
    path('user', CartView.as_view()),
]