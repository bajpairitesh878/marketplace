from .models import *
from rest_framework import serializers
from product_app.serializer import *
from accounts.serializers import *


class UserCartSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserCart
        fields = '__all__'


class CartDetailSerializer(serializers.ModelSerializer):
    product = ProductSerializer(many=True)
    user = UserDetailsSerializer()

    class Meta:
        model = UserCart
        fields = '__all__'
