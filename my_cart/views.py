from rest_framework.views import APIView
from rest_framework.response import Response
from .serializer import *
from .models import *
from accounts.models import *
from accounts.serializers import *
from product_app.models import *
from product_app.serializer import *


# Create your views here.
class CartView(APIView):

    def get(self, request, id):
        query = UserCart.objects.filter(user=id)
        serializer = CartDetailSerializer(query, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = UserCartSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)

    def delete(self, request, id):
        query = UserCart.objects.filter(pk=id).delete()
        return Response({'message':'Successfully Deleted'})
