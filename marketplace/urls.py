"""marketplace URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from utils.routers import DefaultRouter
from django.views.generic import TemplateView
from django.conf.urls import url
from accounts.views import LoginApiView, RegisterApiView

router = DefaultRouter()

urlpatterns = [
    path('admin/', admin.site.urls),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('', include(router.urls)),
    path('api/account/login', LoginApiView.as_view()),
    path('api/account/registration', RegisterApiView.as_view()),
    path('api/accounts/', include('rest_auth.urls')),
    path('api/banners/', include('banners_app.urls')),
    path('api/product/', include('product_app.urls')),
    path('api/profile/', include('accounts.urls')),
    path('api/cart/', include('my_cart.urls')),
    path('api/order/', include('my_orders.urls')),
    path('api/wishlist/', include('my_wishlist.urls')),
    path('api/registrations/', include('rest_auth.registration.urls')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += [path('__debug__/', include(debug_toolbar.urls)),]

admin.site.site_title = ""
admin.site.site_header = " Administration"
admin.site.index_title = "Welcome to Admin Portal"
